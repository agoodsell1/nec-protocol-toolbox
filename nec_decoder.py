#In case the inputs are inverted
HIGH_VALUE = 1
LOW_VALUE = 0 
TIME_COLUMN_NUM = 0 #Please ensure this is the case
DEFAULT_LOCATION = "C:\\Users\\Alex\\Desktop\\temp\\red_btn.csv"
TOLERANCE = 0.0001 #seconds
#Below is used to focus on one area of data
START_TIME = -0.02 #seconds
STOP_TIME = 0.07 #seconds

def in_tolerance(actual_value, set_value):
    """Check if the actual value lies in the tolerance of the set_value
    in_tolerance(float, float) -> bool
    """
    return ((set_value + TOLERANCE) >= actual_value) and ((set_value - TOLERANCE) <= actual_value)


def load_and_decode_edge_transitions(filename):
    """Load the filename, read the timestamps (where a transition occurs) and
    store the timestamps in a list.
    load_file(str) -> list(list(float), list(float), ...)
    """
    f = open(filename, "r")
    contents = f.read()
    f.close()

    #lines == the cleaned up contents split by newline
    lines = [line.strip() for line in contents.strip().split('\n')]

    #save the first values read by the logic analyser
    #(needed to detect the edge transitions)
    current_values = [int(l) for l in lines[0].split(',')[1:]]
    print(current_values)

    #temp variable to hold timestamps of transitions for all channels
    timestamps = []
    for channel in lines[0].split(',')[1:]:
        timestamps.append([])

    #go through each line - if the channel value != the current_values then we
    #    have a transition and should record it.
    for line in lines:
        parts = line.split(',')

        if (float(parts[0]) < START_TIME):
            continue
        elif (float(parts[0]) > STOP_TIME):
            break
        
        time_value = parts[0]
        channels = parts[1:]

        for index, channel in enumerate(channels):
            #transition
            if int(channel) != current_values[index]:
                timestamps[index].append(float(time_value))
                current_values[index] = int(channel)

    return timestamps


def decode_timestamps(timestamps):
    """Validate the timestamps and convert the contained data into binary. If
    the timestamps are not valid, an error string will be put into the
    corresponding channel's list, but the function will decode the remaining
    channels.
    decode_timestamps(list(list(float), list(float), ...)) -> list(list(int), ...)
    """
    #container for decoded data
    decoded_data = []
    for i in range(len(timestamps)):
        decoded_data.append([])

    for index, channel in enumerate(timestamps):
        #check starting burst
        if not in_tolerance(abs(channel[0] - channel[1]), 0.009):
            decoded_data[index] = "Starting Burst Invalid (not 9 ms)"
            continue

        #check starting space
        if not in_tolerance(abs(channel[1] - channel[2]), 0.0045):
            decoded_data[index] = "Starting Space Invalid (not 4.5 ms)"
            continue

        #check remaining data bits
        #want to stop at the second last one
        for data_ts, next_data_ts, thrd_data_ts in zip(channel[2:-2:2], 
                                                       channel[3:-1:2], 
                                                       channel[4::2]):
            #should start with a 0.5625 ms Burst
            if not in_tolerance(abs(data_ts - next_data_ts), 0.0005625):
                decoded_data[index] = "Detected incorrect data bit format"
                break

            #if the next difference is within tolerance of 562.5 us, we'll assume
            #the data bit was a 0, otherwise we'll test for 1.
            if in_tolerance(abs(next_data_ts - thrd_data_ts), 0.0005625):
                decoded_data[index].append(0)

            elif in_tolerance(abs(next_data_ts - thrd_data_ts), 0.0016875):
                decoded_data[index].append(1)

            else:
                print(data_ts, next_data_ts, thrd_data_ts)
                decoded_data[index] = "Detected invalid data bit"
                break

    return decoded_data
            

if __name__ == "__main__":
    ts = load_and_decode_edge_transitions(DEFAULT_LOCATION)
    data = decode_timestamps(ts)
